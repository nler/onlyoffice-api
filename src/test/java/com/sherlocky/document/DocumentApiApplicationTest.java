package com.sherlocky.document;

import com.sherlocky.document.constant.DocumentConstants;
import com.sherlocky.document.entity.DocumentResponse;
import com.sherlocky.document.service.DocumentCacheService;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;

/**
 * @author: zhangcx
 * @date: 2019/8/13 19:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureMockMvc
public class DocumentApiApplicationTest {
    //@Autowired
    //private MockMvc mvc;
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    @Resource(name="redisTemplate")
    private ValueOperations<Object, Object> valOpsStr;
    @Autowired
    private DocumentCacheService documentCacheService;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    AbstractEnvironment environment;

    @Test
    public void testRedisSerializer() {
        System.out.println("#####################");
        System.out.println(redisTemplate);
        System.out.println(redisTemplate.getDefaultSerializer());
        System.out.println(redisTemplate.getKeySerializer());
        System.out.println(redisTemplate.getValueSerializer());
        System.out.println(documentCacheService);
        System.out.println("#####################");
    }

    @Test
    public void testRedisCacheDocuments() {
        RedisConnection conn = RedisConnectionUtils.getConnection(redisTemplate.getConnectionFactory());
        Jedis jedis = (Jedis) conn.getNativeConnection();
        ScanParams scanParams = new ScanParams().count(10).match(String.format(DocumentConstants.DOCUMENT_REDIS_KEY_PREFIX_FORMAT, "*"));
        // 初始游标为0
        int cursor = 0, count = 0;
        boolean firstScan = true;
        while (cursor != 0 || firstScan) {
            firstScan = false;
            ScanResult<String> result = jedis.scan(cursor, scanParams);
            cursor = result.getCursor();
            // 可能会返回重复的元素，需要手动去重
            Set<Object> keys = new HashSet<>();
            keys.addAll(result.getResult());
            count += keys.size();
            System.out.println(keys);
            List<Object> values = redisTemplate.opsForValue().multiGet(keys);
            values.stream().forEach(System.out::println);
        }
        System.out.println(String.format("总数 = %s", count));
    }

    @Test
    public void testBeforeView() throws Exception {
        String uri = "/api/before/view?path={path}&name={name}";
        String filePath = "G:/tmp/xls.xls";
        String name = "我是中文excel666.xls";
        //this.mvc.perform(post("/api/before/view").param(param[0], param[1])).andExpect(status().isOk());
        DocumentResponse response = restTemplate.postForObject(uri, null, DocumentResponse.class, filePath, name);
        System.out.println(MapUtils.getString((Map) response.getData(), "url"));
    }

    @Test
    public void testBeforeViewList() throws Exception {
        String uri = "/api/before/view?path={path}&name={name}";
        File fo = new File("G:\\tmp");
        File[] docs = fo.listFiles((File f, String filename) -> {
            String ext = StringUtils.lowerCase(FilenameUtils.getExtension(filename));
            if (ArrayUtils.contains(DocumentConstants.FILE_TYPE_SUPPORT_VIEW, ext)) {
                return true;
            }
            return false;
        });
        Arrays.stream(docs).forEach((doc) -> {
            DocumentResponse response = restTemplate.postForObject(uri, null, DocumentResponse.class, doc.getAbsolutePath(), doc.getName());
            System.out.println(MapUtils.getString((Map) response.getData(), "url"));
        });
    }

    // 测试临时目录位置
    @Test
    public void testTmp() {
        String key = "java.io.tmpdir";
        System.out.println(environment.getProperty(key));
        System.out.println(System.getProperty(key));
    }
}
