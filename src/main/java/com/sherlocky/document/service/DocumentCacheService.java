package com.sherlocky.document.service;

import com.sherlocky.document.entity.Document;

/**
 * 文件缓存接口
 * @author: zhangcx
 * @date: 2019/8/9 15:50
 */
public interface DocumentCacheService {
    /**
     * 将文档信息放入缓存
     * @param documentKey
     * @param doc
     * @return
     */
    boolean put(String documentKey, Document doc);

    /**
     * 根据 documentKey 从缓存中获取文档
     * @param documentKey
     * @return
     */
    Document get(String documentKey);

    /**
     * 删除缓存
     * @param documentKey
     */
    void remove(String documentKey);
}
